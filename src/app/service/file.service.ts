import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  API_URL:string = environment.baseUrl;
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  token = JSON.parse(localStorage.getItem('user'))
  
  constructor(private httpClient: HttpClient,public router: Router) { }

  fileUpload(file){
    // const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8' ,'');
    console.log(this.token)
    let header = new HttpHeaders().set(
      "x-access-token",
      this.token
    );
    console.log(header)
    let fileuploadURL = this.API_URL + '/users/save-new-file' ;
   return this.httpClient.post(fileuploadURL, file, {headers:header});
  }

  getFiles(){
    let header = new HttpHeaders().set(
      "x-access-token",
      this.token
    );
    let fileviewURL = this.API_URL + '/users/list-all-files' ;
    return this.httpClient.get(fileviewURL, {headers : header})
  }
}
