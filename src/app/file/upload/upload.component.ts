import { FileService } from './../../service/file.service';
import { AuthService } from './../../service/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  fileName: any;
  file: any;
  filestring: any;

  constructor(private authService: AuthService, private fileService : FileService, private route:Router) { }

  ngOnInit(): void {
  }

  onSelectFile(event){
console.log(event)
this.file = event.target.files[0];
console.log(this.file)

// console.log(this.file)
  }

  upload(){

    const reader = new FileReader();
console.log(reader)
reader.readAsDataURL(this.file);
reader.onload = () => {
    // console.log(reader.result);
    this.filestring = reader.result;
    // console.log(this.file)
    // this.upload(this.file)
    let files = {
      file : this.filestring
    }

    this.fileService.fileUpload(files).subscribe(res => {
      console.log(res)
      alert(res['message'])
      this.route.navigateByUrl("/file/preview")
      
    })
};
  
  }

  next(){
    this.route.navigateByUrl('/file/preview')
  }

  logout(){
    this.authService.logout()
  }
  

}
