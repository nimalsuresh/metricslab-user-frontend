import { AuthService } from './../../service/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
name:any;
email:any;
password:any;
emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  register(registerData){
console.log(registerData)
   
let data = {
  name: registerData.name,
  email:registerData.email,
  password:registerData.password
}
this.authService.register(data).subscribe(res => {
  console.log(res)
  if(res){
    alert(res['message'])
  }
})
  }

}
